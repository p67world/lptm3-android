package com.nebuleo.lptm3control;

import android.app.Application;
import android.content.Context;

/**
 * Created by Gilles on 21/05/2017.
 */

public class lptm3control extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        lptm3control.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return lptm3control.context;
    }
}
