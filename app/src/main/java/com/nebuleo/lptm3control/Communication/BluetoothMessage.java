package com.nebuleo.lptm3control.Communication;

/**
 * Created by Gilles on 13/05/2017.
 */

public class BluetoothMessage {
    int     mType;
    int     mTimeoutMs;
    byte[]  mData;

    public BluetoothMessage(int type, byte[] data, int timeoutMs) {
        mType = type;
        mData = data;
        mTimeoutMs = timeoutMs;
    }

    public BluetoothMessage(){
        mType = 0;
        mData = new byte[64];
        mTimeoutMs = 0;
    }

    public int getType(){
        return mType;
    }

    public byte[] getData(){
        return mData;
    }

    public int getTimeoutMs(){
        return mTimeoutMs;
    }
}
