package com.nebuleo.lptm3control;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.UUID;
import android.os.Handler;
import com.nebuleo.lptm3control.dfu.DfuService;

import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;
import no.nordicsemi.android.ble.data.Data;


/**
 * Created by Gilles on 06/08/2015.
 */

// CODE FROM http://developer.android.com/guide/topics/connectivity/bluetooth.html
public class BluetoothCom {
    private final static String TAG = BluetoothCom.class.getSimpleName();

    // UUIDs for LPTm3
    public static UUID GENERIC_ACCESS_UUID = UUID.fromString("00001800-0000-1000-8000-00805f9b34fb");
    public static UUID GENERIC_ATTRIBUTE_UUID = UUID.fromString("00001801-0000-1000-8000-00805f9b34fb");
    public static UUID SECURE_DFU_SERVICE_UUID = UUID.fromString("0000fe59-0000-1000-8000-00805f9b34fb");
    public static UUID LPT_SERVICE_UUID = UUID.fromString("20766e01-2074-706c-206f-656c7562656e");
    public static UUID LPT_DATA_CHAR_UUID = UUID.fromString("00006e02-0000-1000-8000-00805f9b34fb");
    public static UUID LPT_FWVERSION_CHAR_UUID = UUID.fromString("00006e04-0000-1000-8000-00805f9b34fb");
    public static UUID LPT_CHAR_DESC_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    private final BluetoothAdapter mBtAdapter;
    private final Handler mBtHandler;
    private int mBtState;
    private Context mContext;

    private static boolean mMsgFlag = false;

    private static int mFwReadVersion = 0;

    private static boolean bFirstTime = true;

    // BTLE state
    private static BluetoothGatt               mBleGatt = null;
    public BluetoothGattService                mBleLptService = null;
    public BluetoothGattCharacteristic         mBleLptDataChar = null;
    public BluetoothGattCharacteristic         mBleLptCharFwVersion = null;

    // Constants that indicate the current connection state
    public static final int BT_STATE_NONE = 0;       // we're doing nothing
    public static final int BT_STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int BT_STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int BT_STATE_CONNECTED = 3;  // now connected to a remote device
    public static final int BT_STATE_CONNECTION_LOST = 4;  // connection was lost

    private final DfuProgressListener mDfuProgressListener = new DfuProgressListener() {
        @Override
        public void onDeviceConnecting(String deviceAddress) {
            Log.d(TAG, "DFU : connecting");
        }

        @Override
        public void onDeviceConnected(String deviceAddress) {
            Log.d(TAG, "DFU : connected");
        }

        @Override
        public void onDfuProcessStarting(String deviceAddress) {
            Log.d(TAG, "DFU : starting DFU");

            // Give the new state to the Handler so the UI Activity can update
            mBtHandler.obtainMessage(Constants.DFU_STARTING).sendToTarget();
        }

        @Override
        public void onDfuProcessStarted(String deviceAddress) {
            Log.d(TAG, "DFU : DFU started");
            mBtHandler.obtainMessage(Constants.DFU_STARTED).sendToTarget();
        }

        @Override
        public void onEnablingDfuMode(String deviceAddress) {
            Log.d(TAG, "DFU : starting bootloader");
        }

        @Override
        public void onProgressChanged(String deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal) {
            Log.d(TAG, "DFU : progress="+percent+"%");
            // Give the new state to the Handler so the UI Activity can update
            mBtHandler.obtainMessage(Constants.DFU_PROGRESS, 1, -1, percent).sendToTarget();
        }

        @Override
        public void onFirmwareValidating(String deviceAddress) {
            Log.d(TAG, "DFU : validating");
        }

        @Override
        public void onDeviceDisconnecting(String deviceAddress) {
            Log.d(TAG, "DFU : disconnecting");
        }

        @Override
        public void onDeviceDisconnected(String deviceAddress) {
            Log.d(TAG, "DFU : disconnected");
        }

        @Override
        public void onDfuCompleted(String deviceAddress) {
            Log.d(TAG, "DFU : done");
            mBtHandler.obtainMessage(Constants.DFU_COMPLETED).sendToTarget();

        }

        @Override
        public void onDfuAborted(String deviceAddress) {
            Log.d(TAG, "DFU : aborted");
        }

        @Override
        public void onError(String deviceAddress, int error, int errorType, String message) {
            Log.d(TAG, "DFU : error => "+message);
        }
    };

    // Constructor
    public BluetoothCom(Context context, Handler handler){
        mContext = context;
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mBtState = BT_STATE_NONE;
        mBtHandler = handler;

        DfuServiceListenerHelper.registerProgressListener(mContext, mDfuProgressListener);
    }

    // Set the current state
    private synchronized void setState(int state){
        Log.d(TAG, "setState() " + mBtState + " -> " + state);
        mBtState = state;

        // Give the new state to the Handler so the UI Activity can update
        mBtHandler.obtainMessage(Constants.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    // Return the current connection state
    public synchronized int getState() {
        return mBtState;
    }

    // Start the ConnectThread to initiate a connection to a remote device.
    public synchronized void connect(String device) {
        Log.d(TAG, "connect to: " + device);

        // SI BLE => connect
        mBleGatt = mBtAdapter.getRemoteDevice(device).connectGatt(mContext, true, callback);

        setState(BT_STATE_CONNECTING);
    }

    public synchronized void disconnect()
    {
        Log.d(TAG, "DISCONNECT called");
        if(mBleGatt != null)
        {
            mBleGatt.disconnect();
            mBleGatt.close();
        }
    }


    // Stop all threads
    public synchronized void stop() {
        Log.d(TAG, "stop");

        if (mBleGatt != null) {
            // For better reliability be careful to disconnect and close the connection.
            mBleGatt.disconnect();
            mBleGatt.close();
            mBleGatt = null;
        }
        setState(BT_STATE_NONE);
    }

    // Main BTLE device callback where much of the logic occurs.
    private BluetoothGattCallback callback = new BluetoothGattCallback() {
        // Called whenever the device connection state changes, i.e. from disconnected to connected.
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == BluetoothGatt.STATE_CONNECTED) {
                Log.d(TAG, "Connected!");
                // Discover services.
                if (!gatt.discoverServices()) {
                    Log.d(TAG, "Failed to start discovering services!");
                }
            }
            else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                Log.d(TAG, "Disconnected!");
                setState(BT_STATE_CONNECTION_LOST);
            }
            else {
                Log.d(TAG, "Connection state changed.  New state: " + newState);
            }
        }

        // Called when services have been discovered on the remote device.
        // It seems to be necessary to wait for this discovery to occur before
        // manipulating any services or characteristics.
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "Service discovery completed!");
                List<BluetoothGattService> servicesList = gatt.getServices();
                for (int i = 0; i < servicesList.size(); i++) {
                    BluetoothGattService bluetoothGattService = servicesList.get(i);
                    Log.d(TAG, bluetoothGattService.getUuid().toString());
                }

            }
            else {
                Log.d(TAG, "Service discovery failed with status: " + status);
            }

            // Check if firmware is known
            if(gatt.getService(LPT_SERVICE_UUID) != null) {
                if (gatt.getService(LPT_SERVICE_UUID).getCharacteristics() != null) {
                    mBleLptService = gatt.getService(LPT_SERVICE_UUID);

                    List<BluetoothGattCharacteristic> charList = mBleLptService.getCharacteristics();
                    for (int i = 0; i < charList.size(); i++) {
                        BluetoothGattCharacteristic bluetoothGatChar = charList.get(i);
                        Log.d(TAG, bluetoothGatChar.getUuid().toString());
                    }

                    mBleLptCharFwVersion = mBleLptService.getCharacteristic(LPT_FWVERSION_CHAR_UUID);

                    if (mBleLptCharFwVersion == null) {
                        Log.d(TAG, "FW version characteristic is unknown => UPDATE FW FIRST!");

                        manageDfu();
                    } else {
                        // Get characteristics
                        mBleLptDataChar = mBleLptService.getCharacteristic(LPT_DATA_CHAR_UUID);

                        // Read FW version characteristic
                        gatt.readCharacteristic(mBleLptCharFwVersion);

                    }
                } else {
                    manageDfu();
                }
            }
            else{
                manageDfu();
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);

            if(characteristic == mBleLptCharFwVersion)
            {
                mFwReadVersion = characteristic.getIntValue(Data.FORMAT_UINT8, 0);

                Log.d(TAG, "FW version characteristic read. Value = " + mFwReadVersion);


                // TODO clean!
                try{
                    Resources res = mContext.getResources();
                    InputStream in_s;
                    int  FwVersion;

                    in_s = res.openRawResource(R.raw.lptm3_releasenotes);

                    if(in_s != null) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(in_s));
                        String mReadVal = in.readLine();
                        FwVersion = Integer.parseInt(mReadVal);
                        in.close();

                        Log.d(TAG, "FW version theoric = " + FwVersion);

                        if(mFwReadVersion != FwVersion)
                        {
                            manageDfu();
                        } else {
                            // Set state
                            setState(BT_STATE_CONNECTED);

                            gatt.readCharacteristic(mBleLptDataChar);
                        }
                    }
                } catch(Exception e) {
                    Log.e(TAG, "Error reading release notes");
                }



            }
            else if(characteristic == mBleLptDataChar){
                Log.d(TAG, "Data characteristic read. Value = " + characteristic.getIntValue(Data.FORMAT_UINT8, 0));

                // Give the new state to the Handler so the UI Activity can update
                mBtHandler.obtainMessage(Constants.MESSAGE_READ, 1, -1, characteristic.getIntValue(Data.FORMAT_UINT8, 0)).sendToTarget();

                // TODO : clean => not very safe to do like this
                if(bFirstTime) {
                    if (!gatt.setCharacteristicNotification(mBleLptDataChar, true)) {
                        Log.d(TAG, "Couldn't set notifications for data service");
                    }

                    // Next update the RX characteristic's client descriptor to enable notifications.
                    if (mBleLptDataChar.getDescriptor(LPT_CHAR_DESC_UUID) != null) {
                        BluetoothGattDescriptor desc = mBleLptDataChar.getDescriptor(LPT_CHAR_DESC_UUID);
                        desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        if (!gatt.writeDescriptor(desc)) {
                            Log.d(TAG, "Couldn't write data client descriptor value!");
                        }
                    } else {
                        Log.d(TAG, "Couldn't get data client descriptor!");
                    }

                    bFirstTime = false;
                }
            }
        }

        // Called when a remote characteristic changes (like the RX characteristic).
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            if(characteristic == mBleLptDataChar){
                Log.d(TAG, "Data characteristic read. Value = " + characteristic.getIntValue(Data.FORMAT_UINT8, 0));

                // Give the new state to the Handler so the UI Activity can update
                mBtHandler.obtainMessage(Constants.MESSAGE_READ, 1, -1, characteristic.getIntValue(Data.FORMAT_UINT8, 0)).sendToTarget();
            }
        }



        public void manageDfu()
        {
            Log.d(TAG, "ENTERING DFU!");

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                DfuServiceInitiator.createDfuNotificationChannel(mContext);
            }

            final boolean keepBond = false;
            final boolean forceDfu = true; // Sure?
            final boolean enablePRNs = false;
            int numberOfPackets = 12;

            final DfuServiceInitiator starter = new DfuServiceInitiator(mBleGatt.getDevice().getAddress())
                    .setDeviceName(mBleGatt.getDevice().getName())
                    .setKeepBond(keepBond)
                    .setForceDfu(forceDfu)
                    .setPacketsReceiptNotificationsEnabled(enablePRNs)
                    .setPacketsReceiptNotificationsValue(numberOfPackets)
                    .setUnsafeExperimentalButtonlessServiceInSecureDfuEnabled(true);

            starter.setZip(Uri.parse("android.resource://" + mContext.getPackageName() + "/raw/lptm3_app_pkg"), null);

            starter.setScope(DfuServiceInitiator.SCOPE_APPLICATION);

            starter.start(mContext, DfuService.class);
        }
    };

    public int getFirmwareVersion(){
        return mFwReadVersion;
    }

    public void writeLptData(int data)
    {
        Log.d(TAG, "Data written : " + data);
        mBleLptDataChar.setValue(data, Data.FORMAT_UINT8, 0);
        mBleGatt.writeCharacteristic(mBleLptDataChar);
    }
}
