package com.nebuleo.lptm3control.scanner;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import com.nebuleo.lptm3control.BluetoothCom;
import com.nebuleo.lptm3control.Device;
import com.nebuleo.lptm3control.DeviceAdapter;
import com.nebuleo.lptm3control.MainActivity;
import com.nebuleo.lptm3control.R;
import com.nebuleo.lptm3control.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class ScanActivity extends AppCompatActivity {
    private static final long SCAN_PERIOD = 20000;  // Scanning period in ms
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_ENABLE_LOC = 2;
    public static UUID NRF52_SERVICE_UUID = UUID.fromString("20767273-2074-706c-206f-656c7562656e");

    private BluetoothAdapter    mBTAdapter;
    private Handler             mHandler;
    private BluetoothLeScanner  mLEScanner;
    private ScanSettings        mLEScanSettings;
    private List<ScanFilter>    mLEScanFilters;
    private List<Device>        mDeviceList;
    private GridView            mDeviceGridview;
    private DeviceAdapter mDeviceAdapter;
    private boolean             mScanning;
    private SmoothProgressBar   mScanProgressBar;
    private Dialog              mLocationEnableDialog;
    private static final String TAG = ScanActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.lptcontrol_icon);
        if(getSupportActionBar() != null)   getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(v -> openAboutDialog());

        // Handler
        mHandler = new Handler();

        // Listview
        mDeviceGridview = findViewById(R.id.listView);
        initDeviceList();

        // Progress bar
        initProgressBar();

        // ******* VERIF PRESENCE BT ********
        if     ((!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) &&
                (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH))){
            Toast.makeText(this, R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }
        // ************

        // ******** VERIF PERMISSION LOCALISATION ******
        // Android M Permission check
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ENABLE_LOC);
        }

        // On vérifie si la localisation est activée
        if(!Util.isLocationEnabled(this)){
            showLocationEnableDialog();
        }
        // ************

        // Initialisation du BT Adapter
        final BluetoothManager bluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        mBTAdapter = bluetoothManager.getAdapter();

        // Vérification si l'adapter est bien initialisé
        if(mBTAdapter == null){
            Toast.makeText(this, R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scan, menu);

        if(!mScanning) {
            menu.findItem(R.id.statusInfo).setTitle("");
            menu.findItem(R.id.scan_info).setTitle(R.string.scanStart);
        } else {
            menu.findItem(R.id.statusInfo).setTitle(R.string.scanning);
            menu.findItem(R.id.scan_info).setTitle(R.string.scanStatus);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.scan_info:
                if(!mScanning){
                    mDeviceAdapter.clear();
                    scanLeDevice(true);
                }
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ENABLE_LOC: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                } else {
                    showErrorDialog(R.string.error_permission_denied, true);
                }
                return;
            }
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if ((mBTAdapter == null) || (!mBTAdapter.isEnabled())) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            mLEScanner = mBTAdapter.getBluetoothLeScanner();
            mLEScanSettings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                    .build();
            //mLEScanFilters = new ArrayList<>();
            //mLEScanFilters.add(new ScanFilter.Builder().setServiceUuid(new ParcelUuid(NRF52_SERVICE_UUID)).build());

            // Reset liste
            mDeviceAdapter.clear();

            // Lancement scan
            scanLeDevice(true);
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if((mBTAdapter != null) && (mBTAdapter.isEnabled())){
            mDeviceAdapter.clear();
            scanLeDevice(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(mScanTimer, SCAN_PERIOD);
            updateProgressBar(true);
            mScanning = true;
            mLEScanner.startScan(mLEScanFilters, mLEScanSettings, mLeScanCallback);
        } else {
            mHandler.removeCallbacks(mScanTimer);
            mLEScanner.stopScan(mLeScanCallback);
            mScanning = false;
            updateProgressBar(false);
        }
        invalidateOptionsMenu();
    }

    private void initDeviceList()
    {
        mDeviceList = new ArrayList<>();
        mDeviceAdapter = new DeviceAdapter(ScanActivity.this, mDeviceList);
        mDeviceGridview.setAdapter(mDeviceAdapter);

        initialiseListviewListener(mDeviceGridview);
    }

    private void initialiseListviewListener(GridView listView)
    {
        listView.setOnItemClickListener((parent, view, position, id) -> {
            if (mDeviceList.get(position) == null) return;

            // Stop scanning
            scanLeDevice(false);

            // Get intent to send
            final Intent intent = new Intent(ScanActivity.this, MainActivity.class);

            intent.putExtra(MainActivity.EXTRAS_DEVICE_NAME, mDeviceList.get(position).getDeviceName());
            intent.putExtra(MainActivity.EXTRAS_DEVICE_ADDRESS, mDeviceList.get(position).getDeviceAddress());

            Log.d(TAG, "******* Connecting to BT device **************" + mDeviceList.get(position).getDeviceName());
            startActivity(intent);
        });
    }

    private Runnable mScanTimer = () -> scanLeDevice(false);


    // Device scan callback.
    private ScanCallback mLeScanCallback =
            new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    Log.i("callbackType", String.valueOf(callbackType));
                    Log.i("result", result.toString());
                    final BluetoothDevice device = result.getDevice();
                    runOnUiThread(() -> {
                        if((device != null) && (device.getName() != null))
                        {
                            if(device.getName().contains("LPTM3")) {
                                if (mDeviceAdapter.deviceNameExists(device.getName()) < 0) {
                                    mDeviceAdapter.add(new Device(device.getName(), device.getAddress()));
                                }
                            }
                        }
                    });
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    for (ScanResult sr : results) {
                        Log.i("ScanResult - Results", sr.toString());
                    }
                }

                @Override
                public void onScanFailed(int errorCode) {
                    Log.e("Scan Failed", "Error Code: " + errorCode);
                }
            };

    // Other
    private void openAboutDialog()
    {
        runOnUiThread(() -> Util.makeAboutDialog(ScanActivity.this));
    }

    private void showErrorDialog(final int msgID, final boolean finishOnClose)
    {
        runOnUiThread(() -> Util.showErrorDialog(ScanActivity.this, R.string.error, msgID, finishOnClose));
    }

    private void showLocationEnableDialog()
    {
        runOnUiThread(() -> {
            mLocationEnableDialog = new AlertDialog.Builder(ScanActivity.this, R.style.AboutDialog)
                    .setTitle(R.string.loc_enable_title)
                    .setMessage(R.string.loc_enable_msg)
                    .setPositiveButton(R.string.settings, (dialog, which) -> startLocationEnableIntent())
                    .setNegativeButton(R.string.cancel, (dialog, which) -> {
                        dialog.dismiss();
                        showErrorDialog(R.string.error_permission_denied, true);
                    }).create();
            mLocationEnableDialog.show();

        });
    }

    private void startLocationEnableIntent()
    {
        Log.d(TAG, "Directing user to enable location services");
        Intent enableBtIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_LOC);
    }

    private void initProgressBar()
    {
        mScanProgressBar = findViewById(R.id.progressBar);
        mScanProgressBar.setVisibility(View.VISIBLE);
    }

    private void updateProgressBar(final boolean start)
    {
        runOnUiThread(() -> {
            if (start) {
                mScanProgressBar.progressiveStart();
            } else {
                mScanProgressBar.progressiveStop();
            }
        });
    }

}
