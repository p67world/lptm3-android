package com.nebuleo.lptm3control;

/**
 * Created by Gilles on 14/01/2016.
 */
public class Device {
    private int deviceType;
    private String deviceName;
    private String deviceAddress;

    public Device(String deviceName, String deviceAddress){
        this.deviceName = deviceName;
        this.deviceAddress = deviceAddress;
    }

    public String getDeviceName(){
        return deviceName;
    }

    public void setDeviceName(String deviceName){
        this.deviceName = deviceName;
    }

    public String getDeviceAddress(){
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress){
        this.deviceAddress = deviceAddress;
    }
}
