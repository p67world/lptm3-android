package com.nebuleo.lptm3control;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

import android.os.Handler;

import com.nebuleo.lptm3control.Fragments.ChecklistFragment;
import com.nebuleo.lptm3control.Fragments.GeneralFragment;
import com.nebuleo.lptm3control.Fragments.InterFragment;
import com.nebuleo.lptm3control.Fragments.LagFragment;
import com.nebuleo.lptm3control.Fragments.RmtFragment;

import org.w3c.dom.Text;

import me.angrybyte.numberpicker.view.ActualNumberPicker;

public class MainActivity extends AppCompatActivity {
//public class MainActivity extends FragmentActivity implements ActionBar.TabListener {
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private static final int MAX_RETRIES = 5;

    private String mDeviceName;
    private String mDeviceAddress;

    private static GeneralFragment mGeneralFragment;

    // Adaptateur permet de récupérer le fragment
    FragmentPagerAdapter adapterViewPager;

    // Socket Bluetooth
    private static BluetoothCom mBtCom = null;


    private static AlertDialog mConnectDialog = null;


    // Timeout connexion
    private static Handler mConnTimeoutHnd = null;
    private static Runnable mConnTimeoutTsk = null;


    private static final String TAG = MainActivity.class.getSimpleName();

    // Menu
    private static MenuItem g_firmTxt = null;

    // Delayed connect
    private static Handler mDelayedConnectHnd = null;
    private static Runnable mDelayedConnectTsk = null;

    private static int mRetryCount = 0;

    private static int mDataVal = 0;

    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what){
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1){
                        case BluetoothCom.BT_STATE_CONNECTED:
                            Log.d(TAG, "BT CONNECTED");
                            mRetryCount = MAX_RETRIES;


                            if(g_firmTxt != null)
                            {
                                String str = "Firm.\nv" + mBtCom.getFirmwareVersion();
                                g_firmTxt.setTitle(str);
                            }

                            mConnTimeoutHnd.removeCallbacks(mConnTimeoutTsk);
                            if((mConnectDialog != null) && (mConnectDialog.isShowing())) mConnectDialog.dismiss();
                            break;
                        case BluetoothCom.BT_STATE_CONNECTING:
                            Log.d(TAG, "BT CONNECTING...");

                            if((mConnectDialog == null) || (!mConnectDialog.isShowing()))
                            {
                                mConnectDialog = new AlertDialog.Builder(MainActivity.this, R.style.AboutDialog)
                                        .setCancelable(false)
                                        .setView(MainActivity.this.getLayoutInflater().inflate(R.layout.connection_progress, null))
                                        .create();

                                mConnectDialog.show();
                            }
                            // Timeout dans 10s
                            mConnTimeoutHnd.removeCallbacks(mConnTimeoutTsk);
                            mConnTimeoutHnd.postDelayed(mConnTimeoutTsk, 20000);
                            break;
                        case BluetoothCom.BT_STATE_LISTEN:
                        case BluetoothCom.BT_STATE_NONE:
                            break;
                        case BluetoothCom.BT_STATE_CONNECTION_LOST:
                            Log.d(TAG, "BT DISCONNECTED or UNKNOWN");
                            if(mRetryCount < MAX_RETRIES)
                            {
                                mRetryCount++;
                                Log.d(TAG, "CONNECTION RETRY " + mRetryCount);
                                mBtCom.connect(mDeviceAddress);
                            }
                            else {
                                finishMainActivity();
                            }
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    break;
                case Constants.MESSAGE_READ:
                     mDataVal = (int)msg.obj;
                     Log.d(TAG, "VALUE RECEIVED:" + mDataVal);
                    if(mBtCom.getState() == BluetoothCom.BT_STATE_CONNECTED)mGeneralFragment.setG_checkPrefocus((mDataVal & 0xF0) != 0);
                    if(mBtCom.getState() == BluetoothCom.BT_STATE_CONNECTED)mGeneralFragment.setG_pickerSensitivity(mDataVal & 0x0F);

                     break;
                case Constants.MESSAGE_DEVICE_NAME:
                    Log.d(TAG, msg.getData().getString(Constants.DEVICE_NAME));
                    break;
                case Constants.MESSAGE_TOAST:
                    Log.d(TAG, "MESSAGE_TOAST");
                    Toast.makeText(getApplicationContext(), msg.getData().getString(Constants.TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
                case Constants.DFU_STARTING:
                    mConnTimeoutHnd.removeCallbacks(mConnTimeoutTsk);

                    // TODO : not beautiful at all!
                    TextView dialogText = mConnectDialog.findViewById(R.id.connection_text);
                    if(dialogText != null) dialogText.setText(R.string.ota_status_preparing);
                    else    Log.e(TAG, "dialogText is NULL!");
                    break;
                case Constants.DFU_STARTED:
                    // TODO : not beautiful at all!
                    ProgressBar dialogSpinner = mConnectDialog.findViewById(R.id.connection_spinner);
                    ProgressBar dialogProgress = mConnectDialog.findViewById(R.id.connection_ota_progress);
                    if(dialogSpinner != null) dialogSpinner.setVisibility(View.GONE);
                    if(dialogProgress != null) dialogProgress.setVisibility(View.VISIBLE);
                    break;
                case Constants.DFU_PROGRESS:
                    mDataVal = (int)msg.obj;
                    String text = getString(R.string.ota_status_programming) + "(" + mDataVal + "%)";
                    TextView dialogText2 = mConnectDialog.findViewById(R.id.connection_text);
                    ProgressBar dialogProgress2 = mConnectDialog.findViewById(R.id.connection_ota_progress);
                    if(dialogText2 != null) dialogText2.setText(text);
                    if(dialogProgress2 != null) dialogProgress2.setProgress(mDataVal);
                    break;
                case Constants.DFU_COMPLETED:
                    TextView dialogText3 = mConnectDialog.findViewById(R.id.connection_text);
                    ProgressBar dialogSpinner3 = mConnectDialog.findViewById(R.id.connection_spinner);
                    ProgressBar dialogProgress3 = mConnectDialog.findViewById(R.id.connection_ota_progress);
                    if(dialogText3 != null) dialogText3.setText(R.string.progress_title);
                    if(dialogSpinner3 != null) dialogSpinner3.setVisibility(View.VISIBLE);
                    if(dialogProgress3 != null) dialogProgress3.setVisibility(View.GONE);
                    finishMainActivity();
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGeneralFragment = new GeneralFragment();
        mGeneralFragment.setOnGeneralFragmentChangeListener(new GeneralFragment.OnGeneralFragmentChangeListener() {
            @Override
            public void onPrefocusChecked(boolean b) {
                Log.d(TAG, "Prefocus : " + (b?"checked":"unchecked"));
                mDataVal = (mDataVal & 0x0F) | ((b?1:0) << 4);
                mBtCom.writeLptData(mDataVal);
            }

            @Override
            public void onSensitivityChange(int val) {
                Log.d(TAG, "Sensitivity : "+ val);
                mDataVal = (mDataVal & 0xF0) | (val & 0x0F);
                mBtCom.writeLptData(mDataVal);
            }
        });

        // BT
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mRetryCount = 0;

        // Création du pager adapter
        final CustomViewPager pager = findViewById(R.id.pager);
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapterViewPager);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.lptcontrol_icon);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(v -> openAboutDialog());

        // Give the Tablayout the viewpager
        final TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(pager);
        Objects.requireNonNull(tabLayout.getTabAt(0)).setIcon(R.drawable.settings);
        Objects.requireNonNull(tabLayout.getTabAt(0)).setText(R.string.tab_general);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setIcon(R.drawable.checklist);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setText(R.string.tab_checklist);

        // Création du BluetoothCom
        MyHandler mHandler = new MyHandler();
        mBtCom = new BluetoothCom(this, mHandler);

        // Delayed connect
        mDelayedConnectHnd = new Handler();
        mDelayedConnectTsk = () -> mBtCom.connect(mDeviceAddress);

        // Timeout connexion
        mConnTimeoutHnd = new Handler();
        mConnTimeoutTsk = this::finishMainActivity;

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0:
                        break;
                    case 1:
                        break;
                    default:
                }
                Log.d(TAG, "Page "+position+" selected");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }


        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDelayedConnectHnd.postDelayed(mDelayedConnectTsk, 100);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mConnectDialog != null) mConnectDialog.dismiss();
    }

    private void openAboutDialog()
    {
        runOnUiThread(() -> Util.makeAboutDialog(MainActivity.this));
    }

    @Override
    public void onBackPressed() {
        finishMainActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tab, menu);

        // Création des éléments
        g_firmTxt = menu.findItem(R.id.firmTxt);

        MenuItem mDevName = menu.findItem(R.id.devName);
        String str= mDeviceName;

        mDevName.setTitle(str);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.firmTxt:
                // TODO : ouvrir une fenêtre de dialogue avec
                // TODO : * release note
                // TODO : * version actuelle / version dispo
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter{
        private static int NUM_ITEMS = 2;

        public MyPagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return mGeneralFragment;
                case 1:
                    return new ChecklistFragment();
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }


    }

    private void finishMainActivity()
    {
        if(mConnectDialog != null) mConnectDialog.dismiss();
        mBtCom.disconnect();
        mBtCom.stop();
        mRetryCount = 0;
        finish(); // finish activity
    }

}
