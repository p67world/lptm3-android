package com.nebuleo.lptm3control.Splash;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.nebuleo.lptm3control.R;
import com.nebuleo.lptm3control.scanner.ScanActivity;
import com.nebuleo.lptm3control.Util;

public class SplashScreen extends Activity
{
    private static final long SPLASH_SCREEN_TIMEOUT_MS = 3000;

    private Handler mHandler;
    private Runnable mTimeoutTask;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        TextView versionTextView = findViewById(R.id.version);

        String versionString = String.format("Version %s", Util.getAppVersion(this));
        versionTextView.setText(versionString);

        mHandler = new Handler();
        mTimeoutTask = () -> {
            Intent connectIntent = new Intent(SplashScreen.this, ScanActivity.class);
            SplashScreen.this.startActivity(connectIntent);
            SplashScreen.this.finish();
        };

        final TextView link_company = findViewById(R.id.company_link);
        link_company.setOnClickListener(v -> goToWebsite(getString(R.string.website_company)));
    }



    private void goToWebsite(String url)
    {
        Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        SplashScreen.this.startActivity(websiteIntent);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mHandler.postDelayed(mTimeoutTask, SPLASH_SCREEN_TIMEOUT_MS);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        mHandler.removeCallbacks(mTimeoutTask);//stop timer if going to link
    }
}
