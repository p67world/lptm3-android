package com.nebuleo.lptm3control.Cards;

public class CardValue_object {
    private String title;
    private int value;
    private String unit;
    private int image;

    public CardValue_object(String title, int value, String unit, int image) {
        this.title = title;
        this.value = value;
        this.unit = unit;
        this.image = image;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getValue(){
        return value;
    }

    public void setValue(int value){
        this.value = value;
    }

    public String getUnit(){
        return unit;
    }

    public void setUnit(String unit){
        this.unit = unit;
    }


    public int getImage(){
        return image;
    }

    public void setImage(int image){
        this.image = image;
    }
}
