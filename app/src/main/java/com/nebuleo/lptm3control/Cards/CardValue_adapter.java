package com.nebuleo.lptm3control.Cards;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nebuleo.lptm3control.R;

import java.util.List;

public class CardValue_adapter extends RecyclerView.Adapter<CardValue_viewholder> {
    List<CardValue_object> list;
    private onItemClickListener mListener;

    //ajouter un constructeur prenant en entrée une liste
    public CardValue_adapter(List<CardValue_object> list) {
        this.list = list;
    }

    public interface onItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(onItemClickListener listener){
        mListener = listener;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public CardValue_viewholder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_value,viewGroup,false);
        return new CardValue_viewholder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardValue_viewholder holder, int position) {
        CardValue_object myObject = list.get(position);
        holder.bind(myObject);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
