package com.nebuleo.lptm3control.Cards;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nebuleo.lptm3control.R;

public class CardValue_viewholder extends RecyclerView.ViewHolder{

    private TextView cardValueTitle;
    private TextView cardValueValue;
    private ImageView cardValueImage;
    public View view;

    //itemView est la vue correspondante à 1 cellule
    public CardValue_viewholder(View itemView, final CardValue_adapter.onItemClickListener listener) {
        super(itemView);
        view = itemView;

        //c'est ici que l'on fait nos findView
        cardValueTitle = itemView.findViewById(R.id.card_value_title);
        cardValueValue = itemView.findViewById(R.id.card_value_value);
        cardValueImage = itemView.findViewById(R.id.card_value_image);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            }
        });
    }

    public View getView(){
        return view;
    }


    //puis ajouter une fonction pour remplir la cellule en fonction d'un MyObject
    public void bind(CardValue_object myObject){
        String value = myObject.getValue() + myObject.getUnit();
        cardValueTitle.setText(myObject.getTitle());
        cardValueValue.setText(value);
        cardValueImage.setImageResource(myObject.getImage());
    }
}