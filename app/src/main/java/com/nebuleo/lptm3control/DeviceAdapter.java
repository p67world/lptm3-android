package com.nebuleo.lptm3control;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

/**
 * Created by Gilles on 14/01/2016.
 */
public class DeviceAdapter extends ArrayAdapter<Device> {

    public DeviceAdapter(Context context, List<Device> devices){
        super(context, 0, devices);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_element, parent, false);
        }

        DeviceViewHolder viewHolder = (DeviceViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new DeviceViewHolder();
            viewHolder.name = convertView.findViewById(R.id.element_name);
            convertView.setTag(viewHolder);
            convertView.setMinimumWidth(parent.getWidth());
        }

        Device device = getItem(position);
        if (device != null) {
            viewHolder.name.setText(device.getDeviceName());
        }

        return convertView;
    }

    public int deviceNameExists(String name)
    {
        int count = this.getCount();
        for (int i=0; i<count; i++)
        {
            String nextName = Objects.requireNonNull(this.getItem(i)).getDeviceName();

            if (nextName.equals(name))
            {
                return i;
            }
        }
        return -1;
    }

    private class DeviceViewHolder{
        public TextView name;
    }
}
