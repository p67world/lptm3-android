package com.nebuleo.lptm3control.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nebuleo.lptm3control.R;

public class LagFragment extends Fragment {
    int g_iLagVal = 0;


    //@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View l_view = inflater.inflate(R.layout.fragment_lag, container, false);

        // Création des éléments graphiques
        /*g_btnLag = l_view.findViewById(R.id.btnStartLag);
        g_txtRes = l_view.findViewById(R.id.txtResult);
        g_check0 = l_view.findViewById(R.id.checkBox0);
        g_check1 = l_view.findViewById(R.id.checkBox1);
        g_check2 = l_view.findViewById(R.id.checkBox2);
        g_check3 = l_view.findViewById(R.id.checkBox3);
        g_check4 = l_view.findViewById(R.id.checkBox4);
        g_check5 = l_view.findViewById(R.id.checkBox5);
        g_check6 = l_view.findViewById(R.id.checkBox6);
        g_check7 = l_view.findViewById(R.id.checkBox7);

        // Bouton lancement lag
        g_btnLag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtCom.LPTset(Constants.LPT_SET_START_LAG, 0);
            }
        });

        // Checkboxes
        g_check0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 1 : -1;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });

        g_check1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 2 : -2;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });

        g_check2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 4 : -4;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });

        g_check3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 8 : -8;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });

        g_check4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 16 : -16;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });

        g_check5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 32 : -32;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });

        g_check6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 64 : -64;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });

        g_check7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                g_iLagVal += isChecked ? 128 : -128;
                g_txtRes.setText(Integer.toString(g_iLagVal));
            }
        });*/

        return l_view;
    }
}
