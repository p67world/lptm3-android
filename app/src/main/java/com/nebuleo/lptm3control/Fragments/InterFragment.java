package com.nebuleo.lptm3control.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nebuleo.lptm3control.CircularSeekBar.CircularSeekBar;
import com.nebuleo.lptm3control.R;

import org.w3c.dom.Text;

public class InterFragment extends Fragment {
    private CircularSeekBar circular_seekbar;
    private TextView time_seconds;
    private TextView time_minutes;
    private TextView time_hours;
    private TextView current_selected;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    //@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View l_view = inflater.inflate(R.layout.fragment_inter, container, false);

        circular_seekbar = l_view.findViewById(R.id.inter_seekbar);
        time_seconds = l_view.findViewById(R.id.time_seconds);
        time_minutes = l_view.findViewById(R.id.time_minutes);
        time_hours = l_view.findViewById(R.id.time_hours);
        current_selected = time_seconds;

        circular_seekbar.setOnSwagPointsChangeListener(new CircularSeekBar.OnSwagPointsChangeListener() {
            @Override
            public void onPointsChanged(CircularSeekBar circularSeekBar, int points, boolean fromUser) {
                if(circular_seekbar.getPoints() < 10)
                {
                    current_selected.setText("0" + circular_seekbar.getPoints());
                }
                else {
                    current_selected.setText(String.valueOf(circular_seekbar.getPoints()));
                }
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar circularSeekBar) {

            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar circularSeekBar) {

            }
        });

        time_seconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                time_seconds.setTextColor(getResources().getColor(R.color.title));
                time_minutes.setTextColor(getResources().getColor(R.color.colorAccent));
                time_hours.setTextColor(getResources().getColor(R.color.colorAccent));
                current_selected = time_seconds;
                circular_seekbar.setPoints(Integer.parseInt(current_selected.getText().toString()));
            }
        });

        time_minutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                time_seconds.setTextColor(getResources().getColor(R.color.colorAccent));
                time_minutes.setTextColor(getResources().getColor(R.color.title));
                time_hours.setTextColor(getResources().getColor(R.color.colorAccent));
                current_selected = time_minutes;
                circular_seekbar.setPoints(Integer.parseInt(current_selected.getText().toString()));
            }
        });

        time_hours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                time_seconds.setTextColor(getResources().getColor(R.color.colorAccent));
                time_minutes.setTextColor(getResources().getColor(R.color.colorAccent));
                time_hours.setTextColor(getResources().getColor(R.color.title));
                current_selected = time_hours;
                circular_seekbar.setPoints(Integer.parseInt(current_selected.getText().toString()));
            }
        });

        //progressBarCircle = l_view.findViewById(R.id.progressBarCircle);

        //progressBarCircle.listener

        /*recyclerView = l_view.findViewById(R.layout.fragment_inter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        cardAdapter = new CardValue_adapter(cardValues);

        recyclerView.setAdapter(cardAdapter);

        cardAdapter.setOnItemClickListener(new CardValue_adapter.onItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Log.d("CLICKED", "Position " + position);
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.popup_inter_config);
                dialog.setCancelable(true);

                ImageButton closeButton = dialog.findViewById(R.id.ib_close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });


                dialog.show();*/

                /*LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.popup_inter_config, null);
                popup = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                popup.setElevation(5.0f);
                ImageButton closeButton = popupView.findViewById(R.id.ib_close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popup.dismiss();
                    }
                });*/
        /*    }
        });*/

        // Création des éléments graphiques
        /*g_seekInterStart = l_view.findViewById(R.id.seekInterStart);
        g_seekInterShutter = l_view.findViewById(R.id.seekInterShutter);
        g_seekInterInterval = l_view.findViewById(R.id.seekInterInterval);
        g_seekInterNumber = l_view.findViewById(R.id.seekInterNumber);
        g_btnInterStart = l_view.findViewById(R.id.btnInterStart);

        // Seekbars de réglages
        g_seekInterStart.setMax(60);
        g_seekInterStart.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView text = (TextView) l_view.findViewById(R.id.txtInterStart);
                String val = Integer.toString(progress > 0 ? progress : 1) + " s";
                text.setText(val);
                mBtCom.LPTset(Constants.LPT_SET_INTER_DELAY, progress > 0 ? progress : 1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        g_seekInterShutter.setMax(60);
        g_seekInterShutter.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView text = (TextView) l_view.findViewById(R.id.txtInterShutter);
                String val = Integer.toString(progress > 0 ? progress : 1) + " s";
                text.setText(val);
                mBtCom.LPTset(Constants.LPT_SET_INTER_SHUTTER, progress > 0 ? progress : 1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        g_seekInterInterval.setMax(300);
        g_seekInterInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView text = (TextView) l_view.findViewById(R.id.txtInterInterval);
                String val = Integer.toString(progress > 0 ? progress : 1) + " s";
                text.setText(val);
                mBtCom.LPTset(Constants.LPT_SET_INTER_INTERVAL, progress > 0 ? progress : 1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        g_seekInterNumber.setMax(99);
        g_seekInterNumber.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView text = (TextView) l_view.findViewById(R.id.txtInterNumber);
                text.setText(progress > 0 ? Integer.toString(progress) : "INF");
                mBtCom.LPTset(Constants.LPT_SET_INTER_NB_POSES, progress);
                g_iInterNbPoses = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        g_btnInterStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (g_btnInterStart.getText() ==  getResources().getString(R.string.inter_stop)) {
                    mBtCom.LPTset(Constants.LPT_SET_INTER_STOP, 0);
                } else {
                    mBtCom.LPTset(Constants.LPT_SET_INTER_START, 0);
                }


            }
        });*/

        return l_view;
    }
}
