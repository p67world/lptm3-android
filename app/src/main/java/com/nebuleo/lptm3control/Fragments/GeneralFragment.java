package com.nebuleo.lptm3control.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nebuleo.lptm3control.R;

// Chargement du contenu
public class GeneralFragment extends Fragment {

    private static CheckBox g_checkPrefocus = null;
    private static SeekBar g_seekSensitivity = null;
    private static TextView g_textPrefocus = null;


    OnGeneralFragmentChangeListener callback;

    public void setOnGeneralFragmentChangeListener(OnGeneralFragmentChangeListener callback){
        this.callback = callback;
    }

    public interface OnGeneralFragmentChangeListener{
        void onPrefocusChecked(boolean b);
        void onSensitivityChange(int val);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View l_view = inflater.inflate(R.layout.fragment_general, container, false);

        g_checkPrefocus = l_view.findViewById(R.id.cbprefocus);
        g_seekSensitivity = l_view.findViewById(R.id.general_val_sensitivity);
        g_textPrefocus = l_view.findViewById(R.id.textprefocus);

        g_checkPrefocus.setOnCheckedChangeListener((compoundButton, b) -> {
            callback.onPrefocusChecked(b);
        });

        g_seekSensitivity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(b)
                {
                    callback.onSensitivityChange(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        return l_view;
    }

    public void setG_checkPrefocus(boolean ischecked) {
        g_checkPrefocus.setChecked(ischecked);
        g_textPrefocus.setText(ischecked ? getString(R.string.cb_prefocus_on) : getString(R.string.cb_prefocus_off));
    }

    public void setG_pickerSensitivity(int value)
    {
        g_seekSensitivity.setProgress(value);
    }

    public interface PrefocusChecked{
        void prefocusChecked(boolean checked);
    }
}
