package com.nebuleo.lptm3control.Fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.nebuleo.lptm3control.R;

/**
 * Created by Gilles on 07/05/2017.
 */

public class ChecklistFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View l_view = inflater.inflate(R.layout.fragment_checklist, container, false);

        CheckBox checklist_cb1 = l_view.findViewById(R.id.checklist_cb1);
        CheckBox checklist_cb2 = l_view.findViewById(R.id.checklist_cb2);
        CheckBox checklist_cb3 = l_view.findViewById(R.id.checklist_cb3);
        CheckBox checklist_cb4 = l_view.findViewById(R.id.checklist_cb4);
        CheckBox checklist_cb5 = l_view.findViewById(R.id.checklist_cb5);
        CheckBox checklist_cb6 = l_view.findViewById(R.id.checklist_cb6);
        CheckBox checklist_cb7 = l_view.findViewById(R.id.checklist_cb7);
        CheckBox checklist_cb8 = l_view.findViewById(R.id.checklist_cb8);

        final TextView checklist_t1 = l_view.findViewById(R.id.checklist_t1);
        final TextView checklist_t2 = l_view.findViewById(R.id.checklist_t2);
        final TextView checklist_t3 = l_view.findViewById(R.id.checklist_t3);
        final TextView checklist_t4 = l_view.findViewById(R.id.checklist_t4);
        final TextView checklist_t5 = l_view.findViewById(R.id.checklist_t5);
        final TextView checklist_t6 = l_view.findViewById(R.id.checklist_t6);
        final TextView checklist_t7 = l_view.findViewById(R.id.checklist_t7);
        final TextView checklist_t8 = l_view.findViewById(R.id.checklist_t8);

        final int checkedColor = getResources().getColor(R.color.textColorPrimary);
        final int uncheckedColor = getResources().getColor(R.color.colorAccent);

        checklist_cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t1.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        checklist_cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t2.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        checklist_cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t3.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        checklist_cb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t4.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        checklist_cb5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t5.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        checklist_cb6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t6.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        checklist_cb7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t7.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        checklist_cb8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checklist_t8.setTextColor(isChecked?checkedColor:uncheckedColor);
            }
        });

        return l_view;
    }
}
