package com.nebuleo.lptm3control.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nebuleo.lptm3control.R;

public class RmtFragment extends Fragment {

    //@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View l_view = inflater.inflate(R.layout.fragment_remotectrl, container, false);

        // Création des éléments graphiques
        //g_rmtChrono = (Chronometer)l_view.findViewById(R.id.chrono);

        return l_view;
    }
}
