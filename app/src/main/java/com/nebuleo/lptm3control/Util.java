/*
 * Copyright (C) 2015, Zentri, Inc. All Rights Reserved.
 *
 * The Zentri BLE Android Libraries and Zentri BLE example applications are provided free of charge
 * by Zentri. The combined source code, and all derivatives, are licensed by Zentri SOLELY for use
 * with devices manufactured by Zentri, or devices approved by Zentri.
 *
 * Use of this software on any other devices or hardware platforms is strictly prohibited.
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.nebuleo.lptm3control;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Util
{
    public static AlertDialog makeAboutDialog(Activity activity)
    {
        LayoutInflater inflater = activity.getLayoutInflater();

        AlertDialog dialog = new AlertDialog.Builder(activity, R.style.AboutDialog)
                .setTitle(R.string.about)
                .setCancelable(true)
                .setView(inflater.inflate(R.layout.about_dialog, null))
                .create();

        return setAboutDialogContentsAndShow(activity, dialog);
    }

    private static AlertDialog setAboutDialogContentsAndShow(final Activity activity, AlertDialog dialog)
    {
        String app_name = activity.getString(R.string.app_name);
        //String link = activity.getString(R.string.code_link_display);
        String text = activity.getString(R.string.about_text);
        String version = getAppVersion(activity.getApplicationContext());

        String msg = String.format("%s v%s\n%s", app_name, version, text);

        dialog.show();//must show to be able to find views
        TextView messageView = dialog.findViewById(R.id.about_body);
        messageView.setText(msg);

        final TextView linkView = dialog.findViewById(R.id.about_link);
        //linkView.setText(link);
        linkView.setOnClickListener(v -> {
            //go to webpage
            Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(activity.getString(R.string.website_company)));
            activity.startActivity(websiteIntent);//open browser
        });

        dialog.setTitle(Html.fromHtml("<font color='" +
                                        activity.getResources().getColor(R.color.colorAccent) +
                                        "'>" +
                                        activity.getResources().getString(R.string.about) +
                                        "</font>"));

        //remove divider in jellybean
        Resources res = activity.getResources();
        //setTitleColour(res, dialog, R.color.p67blue);
        setDividerColour(res, dialog, R.color.transparent);

        return dialog;
    }


    public static String getAppVersion(Context context)
    {
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();

        String versionName = ""; // initialize String

        if (packageManager != null && packageName != null)
        {
            try
            {
                versionName = packageManager.getPackageInfo(packageName, 0).versionName;
            }
            catch (PackageManager.NameNotFoundException e)
            {
            }
        }

        return versionName;
    }

    public static Dialog showErrorDialog(final Activity activity, final int titleID,
                                         final int msgID, final boolean finishOnClose)
    {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        if (!activity.isFinishing())//make sure activity not finishing before showing
        {
            dialog = builder.setCancelable(false)
                    .setTitle(titleID)
                    .setMessage(msgID)
                    .setPositiveButton(R.string.ok, (dialog1, which) -> {
                        dialog1.dismiss();
                        if (finishOnClose)
                        {
                            activity.finish();
                        }
                    }).create();
            dialog.show();

            Resources res = activity.getResources();

            //setTitleColour(res, dialog, R.color.p67blue);
            //setDividerColour(res, dialog, R.color.transparent);
        }

        return dialog;
    }


    public static boolean isLocationEnabled(Context context)
    {
        int locationMode = Settings.Secure.LOCATION_MODE_OFF;
        try
        {
            locationMode = Settings.Secure.getInt(context.getContentResolver(),
                    Settings.Secure.LOCATION_MODE);

        } catch (Settings.SettingNotFoundException e)
        {
            e.printStackTrace();
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    static void setDividerColour(Resources res, Dialog dialog, int colourID)
    {
        final View divider = getElement(res, "titleDivider", dialog);
        int color = res.getColor(colourID);
        if (divider != null)
        {
            divider.setBackgroundColor(color);
        }
    }

    static void setTitleColour(Resources res, Dialog dialog, int colourID)
    {
        final TextView title = (TextView)getElement(res, "alertTitle", dialog);
        int color = res.getColor(colourID);
        if (title != null)
        {
            title.setTextColor(color);
        }
    }

    private static View getElement(Resources res, String id, Dialog dialog)
    {
        final int element_id = res.getIdentifier(id, "id", "android");
        return dialog.findViewById(element_id);
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String bytesToHex(byte[] bytes, int len) {
        char[] hexChars = new char[len * 2];
        for ( int j = 0; j < len; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static void calcLPTpwm(byte[] values)
    {
        for(int i = 0; i < 6; i++)
        {
            double l_dLog = (1-Math.log10(11 - (i * 2)))*100;
            if(l_dLog < 0.) l_dLog = 0;
            else if(l_dLog > 100) l_dLog = 100;
            values[i] = (byte) l_dLog;
        }
    }
}
