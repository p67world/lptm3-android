10
03
Corrected a bug with update on iOS
02
Correction of unwanted shutter controls
Correction of push button long press that worked only after a short press
Optimized day/night sensitivity automatic switching level
Modified sensitivities levels
01
Added software revision characteristic
00
Initial release